#FROM elastic/logstash:7.13.3
FROM scratch
LABEL maintaner="Wolfgang Huse <wolfgang.huse@nutanix.com>"

COPY . .

EXPOSE 8080

CMD ["./main"]
