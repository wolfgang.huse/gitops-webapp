package main

import (
	"os"
	"fmt"
	"net/http"
)

func infoHandler(w http.ResponseWriter, r *http.Request) {
	
	hostname, err := os.Hostname()
	if err != nil {
		fmt.Println(fmt.Errorf("Error: %v", err))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write([]byte("local hostname is: " + hostname))
	
}