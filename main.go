package main

import (
	"database/sql"
	"net/http"
	"os"
	"fmt"
	"strings"
	"context"
	"log"
	"net"
	//"net/url"
	"time"

	minio "github.com/minio/minio-go/v7"
	//"github.com/minio/minio-go/v7/pkg/credentials"
	//"github.com/minio/minio-go/v7/pkg/s3utils"

	"github.com/ianschenck/envflag"
	
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

// S3 - A S3 implements FileSystem using the minio client
// allowing access to your S3 buckets and objects.
//
// Note that S3 will allow all access to files in your private
// buckets, If you have any sensitive information please make
// sure to not sure this project.
type S3 struct {
	*minio.Client
	bucket string
}
func pathIsDir(ctx context.Context, s3 *S3, name string) bool {
	name = strings.Trim(name, pathSeparator) + pathSeparator
	listCtx, cancel := context.WithCancel(ctx)

	objCh := s3.Client.ListObjects(listCtx,
		s3.bucket,
		minio.ListObjectsOptions{
			Prefix:  name,
			MaxKeys: 1,
		})
	for range objCh {
		cancel()
		return true
	}
	return false
}

// Open - implements http.Filesystem implementation.
func (s3 *S3) Open(name string) (http.File, error) {
	if name == pathSeparator || pathIsDir(context.Background(), s3, name) {
		return &httpMinioObject{
			client: s3.Client,
			object: nil,
			isDir:  true,
			bucket: bucket,
			prefix: strings.TrimSuffix(name, pathSeparator),
		}, nil
	}

	name = strings.TrimPrefix(name, pathSeparator)
	
	obj, err := getObject(context.Background(), s3, name)
	if err != nil {
		return nil, os.ErrNotExist
	}

	return &httpMinioObject{
		client: s3.Client,
		object: obj,
		isDir:  false,
		bucket: bucket,
		prefix: name,
	}, nil
}

func getObject(ctx context.Context, s3 *S3, name string) (*minio.Object, error) {
	names := [4]string{name, name + "index.html", name + "index.htm", "404.html"}
	for _, n := range names {
		obj, err := s3.Client.GetObject(ctx, s3.bucket, n, minio.GetObjectOptions{})
		if err != nil {
			log.Println(err)
			continue
		}

		_, err = obj.Stat()
		if err != nil {
			// do not log "file" in bucket not found errors
			if minio.ToErrorResponse(err).Code != "NoSuchKey" {
				log.Println(err)
			}
			continue
		}

		return obj, nil
	}

	return nil, os.ErrNotExist
}

var (
	dbuser		string
	dbpass		string
	dbhost		string
	dbport		string
	dbname		string
	endpoint    string
	accessKey   string
	secretKey   string
	bucket      string
)

func init() {
	envflag.StringVar(&dbuser,"DBUSER","postgres","DB User")
	envflag.StringVar(&dbpass,"DBPASS","nutanix/4u","DB Password")
	envflag.StringVar(&dbhost,"DBHOST","172.23.0.225","DB Host")
	envflag.StringVar(&dbport,"DBPORT","5432","DB Port")
	envflag.StringVar(&dbname,"DBNAME","db","DB Name")
	envflag.StringVar(&endpoint, "ENDPOINT", "http://objects.wooky.xyz", "S3 server endpoint")
	envflag.StringVar(&accessKey, "ACCESSKEY", "", "Access key of S3 storage")
	envflag.StringVar(&secretKey, "SECRETKEY", "", "Secret key of S3 storage")
	envflag.StringVar(&bucket, "BUCKET", "media", "Bucket name which hosts static files")
}

// NewCustomHTTPTransport returns a new http configuration
// used while communicating with the cloud backends.
// This sets the value for MaxIdleConnsPerHost from 2 (go default)
// to 100.
func NewCustomHTTPTransport() *http.Transport {
	return &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		MaxIdleConns:          1024,
		MaxIdleConnsPerHost:   1024,
		IdleConnTimeout:       30 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
		DisableCompression:    true,
	}
}

func main() {


	envflag.Parse()
	if strings.TrimSpace(bucket) == "" {
		log.Fatalln(`Bucket name cannot be empty, please provide 'ENV BUCKET"'`)
	}

	// u, err := url.Parse(endpoint)
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	// var defaultAWSCredProviders = []credentials.Provider{
	// 	&credentials.EnvAWS{},
	// 	&credentials.FileAWSCredentials{},
	// 	&credentials.IAM{
	// 		Client: &http.Client{
	// 			Transport: NewCustomHTTPTransport(),
	// 		},
	// 	},
	// 	&credentials.EnvMinio{},
	// }
	// if accessKey != "" && secretKey != "" {
	// 	defaultAWSCredProviders = []credentials.Provider{
	// 		&credentials.Static{
	// 			Value: credentials.Value{
	// 				AccessKeyID:     accessKey,
	// 				SecretAccessKey: secretKey,
	// 			},
	// 		},
	// 	}
	// }

	// If we see an Amazon S3 endpoint, then we use more ways to fetch backend credentials.
	// Specifically IAM style rotating credentials are only supported with AWS S3 endpoint.
	
	// creds := credentials.NewChainCredentials(defaultAWSCredProviders)

	// client, err := minio.New(u.Host, &minio.Options{
	// 	Creds:        creds,
	// 	Secure:       u.Scheme == "https",
	// 	Region:       s3utils.GetRegionFromURL(*u),
	// 	BucketLookup: minio.BucketLookupAuto,
	// 	Transport:    NewCustomHTTPTransport(),
	// })
	// if err != nil {
	// 	log.Fatalln(err)
	// }


	// Setup connection to our postgresql database

	connString := fmt.Sprintf("port=%s host=%s user=%s "+"password=%s dbname=%s sslmode=disable",dbport, dbhost, dbuser, dbpass, dbname)

	db, err := sql.Open("postgres", connString)
	if err != nil {
		panic(err)
	}

	// Check whether we can access the database by pinging it
	err = db.Ping()
	if err != nil {
		panic(err)
	}

	// Place our opened database into a `dbstruct` and assign it to `store` variable.
	// The `store` variable implements a `Store` interface. The `store` variable was
	// declared globally in `store.go` file.
	store = &dbStore{db: db}

	// Create router

	// Declare a router
	r := mux.NewRouter()
	// Declare static file directory
	// Create static file server for our static files, i.e., .html, .css, etc

	staticFileDirectory := http.Dir("./static/")
	staticFileServer := http.FileServer(staticFileDirectory)
	
	// Create S3 File Service
	//S3FileServer := http.FileServer(&S3{client, bucket})
	
	staticFileHandler := http.StripPrefix("/", staticFileServer)
	//staticFileHandler := http.StripPrefix("/", S3FileServer)
	
	r.Handle("/", staticFileHandler).Methods("GET")
	r.HandleFunc("/person", getPersonHandler).Methods("GET")
	r.HandleFunc("/person", createPersonHandler).Methods("POST")
	r.HandleFunc("/info", infoHandler)

	
	// Listen to the port. Go server's default port is 8080.
	//err = http.ListenAndServe(":8080", r)
	err = http.ListenAndServe(":8080", r)
	if err != nil {
		panic(err)
	}
}
